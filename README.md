# kvm
<pre>
kvm builder tools
--------------------------------------------------------------
This tool will use libvirt and virt-install to provision kvm nodes on Linux.
This code was built and tested on Ubuntu 18.04.
The base images are downloaded from Ubuntu cloud using img files.
The images are configured via cloud-init and a generated cdrom iso.

How To:
-------------------------------------------------------
Build a general purpose node==============================
-------------------------------------------------------
run as root or use sudo
# make -e NAME=foobar.something.org node

will create a default node (general)


To build a standalone gitlab host
-------------------------------------------------------
run as root or use sudo
must have full lvm tools installed, /dev/mapper/vggl-gitlab --> /var/lib/gitlab
root password for website login is in user-data-dir/<DISTRO>/user-data.tmpl and in CLEARTEXT

NOTE:  password must meet gitlab minimum password length.
NOTE:  default network mode is static, the declared FQDN must be declared either in /etc/hosts or DNS.
NOTE:  full lvm tools must be installed
NOTE:  runs as root

# make -e NAME=gitlab.foo.org DATASIZE=8 VCPUS=4 ROLE=gitlab node

This will create an Ubuntu distro (defaults) and allocate a sdc disk, 8GB, and use it for gitlab /var/ directory.

To delete:
-------------------------------------------------------
# make -e NAME=gitlab.foo.ord Delete

Variables:
key variables are:
DISTRO:...(xenial|bionic|focal) (default: focal)
SWAPSIZE:.(GB) (default: 2)
DATASIZE:.(GB) (default: 0)
RAM:......(MB) (default: 2048)
VCPUS:....(COUNT) (default: 2)
NAME:.....fqdn (REQUIRED) 
NET:......(static|dhcp) (default: static)
ROOTSIZE:.(GB) (default: size of image)

Targets
help    --> This help
list    --> List currently defined libvirt nodes
targets --> Produce this listing
stats   --> Display variable info
clean   --> Clean
sources --> Build sources directory
base    --> Build qcow2 base images
image   --> libvirt node images
disks   --> Create node disks
rootfs  --> Create node rootfs disk
swap    --> Create node swap disk
data    --> Create node data disk
Delete  --> Delete node
node    --> Create node


Overview:
=======================================
The ./distro file contains the url of all of the cloud images, and their names.
The initial variables are populated based on the DISTRO defined (from the ./distro file), and disks are created.
All of the virtual nodes rootfs images are qemu-img qcow2 images, using a master backing store.  
Therefore the rootfs are just a collection of changes from a read-only shared master rootfs.  
This keeps the disk sizes down.  You must consider this if you plan on copying an image to another system.
The libvirt tools (virt-install) are used to provision the cloud-image with the selected parameters.
Subdirectories, with children matching the distro name, and a pattern with the rolename, constitute the parameters.
Of interest will be the ./packages-dir/DISTRO/package-rolename files.  
This is where you can add or delete packages when building that "rolename".
Also the runcmd-dir/DISTRO/runcmd-rolename files.  
These are the shell tasks executed on the new node when it is rebooted after the install.


Tasks:

Build a node using defaults:
==============================
make -e NAME=some-node-fqdn node

Build a node with a data disk of 8 gb
==============================
make -e NAME=fqdn DATASIZE=8 node

Build a node from distro xenial
==============================
make -e NAME=fqdn DISTRO=xenial node

Build a node with a Role
==============================
make -e NAME=fqdn ROLE=rolename node

Delete a node
==============================
make -e NAME=fqdn Delete
</pre>
